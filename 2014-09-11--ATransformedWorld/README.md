# A Transformed World | Bogazköy in the First Third of the Second Millennium

This talk was held durign the [EAA in Istanbul](https://www.eaa2014istanbul.org)
the 11 September 2014. 

The session  **T05S014 Times of Changes at Kültepe/Kanes and in Central Anatolia
in the Light of Current Researches** was organised by Fikri Kulakoğlu, Guido
Kryszat and Ryoichi Kontani. Many thanks to them!


## [Slideshow](ATransformedWorld-Beamer.pdf)

The slideshow is written in Rmarkdown for a 
beamer presentation: [ATransformedWorld-Beamer.Rmd](ATransformedWorld-Beamer.Rmd).

 - Images are stored in the [eponyme folder](images)
 - R code is archived in a [rcode folder](rcode) 
 - Data are in [data](data) 
 - Code for OxCal are in the [radiocarbon sequences](radiocarbon) folder
 - The folder [plan](plan) contains the code and the avaiable data for plans 

Along with the rcode, I provide raw [data](data) for the plot of the capacity from
vessels from the MBA. This data comes from: [Strupler, N. Vorratshaltung im
mittelbronzezeitlichen Bogazköy. Spiegel einer häuslichen und regionalen
Ökonomie Istanbuler Mitteilungen, 2013, 63,
17-50](http://orcid.org/0000-0002-2898-6217)

## Abstract 

During the second half of the second millennium BC  (Late Bronze Age)  the site
of Boğazköy / Ḫattuša is known as the capital of the Hittites succeeding to the
City-State of Ḫattuš (Middle Bronze Age). Even if many Old Assyrian and Hittite
cuneiform texts were funded in the first half of the 20th century, the last
decades provided many more archaeological discoveries and therefore new
interpretations. Recent finds and new methodologies modified the chronological
framework with a better time resolution, environmental researches threw light on
the economic organisation of the settlement and reassessment of the remains with
new theories poses a serious challenge on the transformation of the city.
 
The research program started in 2009 at Kesikkaya provides along with the
reassessment of the older excavations from the Lower City a better understanding
of the structural changes from the MBA to LBA at Boğazköy. This paper aims to
clarify this transformation and the restructuring of the domestic quarters
during this period. A focus on the vernacular architecture gives hints at the
planning of the domestic quarters and unveils the choices that led to the
transformation of the city-state into a capital.


