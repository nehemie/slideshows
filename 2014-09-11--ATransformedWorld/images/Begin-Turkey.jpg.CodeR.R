###########################################################
##            MAKING a MAP of TURKEY                     ##
########################################################### 


### This example is inspired from a example from 'Marc in the box'
# contact: ningkiling@gmail.com
# - menugget.blogspot.com 
# - http://stackoverflow.com/users/1199289/marc-in-the-box
#
# The post Importing bathymetry and coastline data in R 
# http://menugget.blogspot.de/2014/01/importing-bathymetry-and-coastline-data.html 
# (last Accessed 23. April 2015)
# Copyright holder: 'Marc in the box' (ningkiling@gmail.com)
# Minor modulations Sept. 2014 by Néhémie Strupler



### required packages
library(RNetCDF)


### Data
# British Oceanographic Data Centre 
# http://www.bodc.ac.uk
#
# Attribution: GEBCO One Minute Grid: ‘The GEBCO One Minute Grid, 
# version 2.0, http://www.gebco.net’

# Download 
## https://www.bodc.ac.uk/data/online_delivery/gebco/gebco_08_grid/
## selected area -25 -35 50 45
## You need to register to download the data, there is no direct link.
## When you have them:
bathy_fname <- "gebco_1min_-25_-35_50_45.nc" 


# Load bathy data
nc <- open.nc(bathy_fname)
print.nc(nc)
tmp <- read.nc(nc)
z <- array(tmp$z, dim=tmp$dim)
# z[which(z > 0)] <- NaN
z <- z[,rev(seq(ncol(z)))]
xran <- tmp$x_range
yran <- tmp$y_range
zran <- tmp$z_range
lon <- seq(tmp$x[1], tmp$x[2], tmp$spac[1])
lat <- seq(tmp$y[1], tmp$y[2], tmp$spac[1])
rm(tmp)
close.nc(nc)

### Plot
# make palette
ocean.pal <- colorRampPalette(
    c("#000000", "#000209", "#000413", "#00061E", "#000728", "#000932", "#002650", 
      "#00426E", "#005E8C", "#007AAA", "#0096C8", "#22A9C2", "#45BCBB", 
      "#67CFB5", "#8AE2AE", "#ACF6A8", "#BCF8B9", "#CBF9CA", "#DBFBDC", 
      "#EBFDED")
)

land.pal <- colorRampPalette(
    c("#336600", "#F3CA89", "#D9A627", 
      "#A49019", "#9F7B0D", "#996600", "#B27676", "#C2B0B0", "#E5E5E5", 
      "#FFFFFF")
)

zbreaks <- seq(-6000, 6000, by=10)
cols <-c(ocean.pal(sum(zbreaks<=0)-1), land.pal(sum(zbreaks>0)))




##############
#### Make JPG
##############
jpeg("Turkey.jpg", width=10, height=6, units="in", res=200)
layout(matrix(1:2, 1,2), widths=c(8,1), heights=c(6))
par(mar=c(2,2,1,1), ps=10)

# Plot image
image(lon, lat, z=z, col=cols, breaks=zbreaks, useRaster=TRUE, ylim=c(30,45), xlim=c(20,50))

# Add text
text(x=34.6, y=40.5, "Boğazköy")
points(x=34.6, y=40, type="p", pch=16)

# Add Legend
par(mar=c(2,0,1,5))
image(x=1, y=zbreaks, z=matrix(zbreaks, 1, length(zbreaks)), col=cols, breaks=zbreaks, useRaster=TRUE, xlab="", ylab="", axes=FALSE)
axis(4, at=seq(-6000, 6000, 1000), las=2)
mtext("meters", side=4, line=3)
box()

dev.off()