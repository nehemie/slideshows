# Readme 

## Format of the CSV

.csv are formatted with:
  - Unicode (UTF-8)
  - Field delimiter {;}
  - Text delimiter {"}
  - text cells are quoted

## Format of the TSV

.tsv are formatted with:
  - Unicode (UTF-8)
  - Field delimiter {tab}
  - Text delimiter {"}


## [MBAPotteryCapacity.tsv](MBAPotteryCapacity.tsv)

This is the data frame for making the box-plot of capacity against shapes [(slide 28/32)](../ATransformedWorld-Beamer.Rmd#how-different-are-these-large-storage-vessels). This data frame is a translation from the [original German data](MBAPotteryCapacity-German.tsv) collected for the article **Strupler 2013**  were more details are provided on the collection. Plotting code is in [Rcode folder ](../Rcode)

The variables are
  - **ID**: identifier for the ceramic
  - **Type**: according to a basic typology
  - **Preservation**: state of preservation of the vessel (whole profiles or fragments) 
  - **Capacity**: volume (in litre)
  - **Dop**: diameter of the rim in cm
  - **Dmin**: minimal diameter of the vessel in cm
  - **Dmax**: maximum diameter of the vessel in cm
  - **Dbase**: diameter of the base in cm
  - **Height**: height of the vessel in cm
  - **Stratigraphie**: kind of layer
  - **Origin**: where the object was found
  - **Reference**: publication of the vessel with drawing

## [CapacityPitGeb.tsv](CapacityPitGeb.tsv)

This data frame is the original from the article **Strupler 2013**


## [TranslationAndEncodingNotes.tsv](TranslationAndEncodingNotes.tsv)

This file provides ontology for the translation from the [ original German terminology ](MBAPotteryCapacity-German.tsv) into an [ English version ](MBAPotteryCapacity.tsv)

## Licence

  - CC BY 4.0, copyrights holder: Néhémie Strupler
  - For citation refers to **Strupler 2013**

## References

   - **Fischer 1963**: [Fischer Franz, Die hethitische Keramik von Boğazköy, 1963](http://amar.hsclib.sunysb.edu/cdm/compoundobject/collection/amar/id/136079)
   - **Orthmann 1984**: Orthmann, Winfried, Keramik aus den ältesten Schichten von Büyükkale, in Bittel, K., Bachmann, H.-G., Naumann, R., Neumann, G., Neve, P., Orthmann, W. & Otten, H. Boğazköy VI. Funde aus den Grabungen bis 1979, 1984, 9-62
   - **Schachner 2010**: Schachner, A. Die Ausgrabungen in Boğazköy-Ḫattuša 2010, Archäologischer Anzeiger 2011, 31-86 
   - **Schachner 2012**: Schachner, A. Die Ausgrabungen in Boğazköy-Ḫattuša 2011, Archäologischer Anzeiger 2012, 85-13 
   - **Strupler 2011**: [Strupler, Néhémie, Vorläufiger Überblick über die Karum-zeitlichen Keramikinventare aus den Grabungen in der südlichen Unterstadt apud Schachner, Andreas, Die Ausgrabungen in 2010, Archäologischer Anzeiger 2011/1, 51 - 57]()
   - **Strupler 2013**: [Strupler Néhémie, Vorratshaltung im mittelbronzezeitlichen Bogazköy. Spiegel einer häuslichen und regionalen Ökonomie Istanbuler Mitteilungen, 2013, 63, 17-50](http://orcid.org/0000-0002-2898-6217)