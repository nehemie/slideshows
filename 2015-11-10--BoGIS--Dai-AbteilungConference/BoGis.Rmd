---
title: "BoGIS Project "
author: '[N. Strupler](mailto:nehemie.strupler@etu.unistra.fr)'
date: "2015-11-13 | Wissenschaftliche Abteilungskonferenz des DAI Istanbul"
output:
  ioslides_presentation:
    fig_height: 4
    fig_width: 7
    highlight: espresso
    incremental: no
    logo: ./Images/Logo-DAI.png
    toc: yes
    transition: 0
    widescreen: yes
subtitle: "Archivierung eines un(meta)dokumentierten GIS"
---
<style>

body {  background-color: black; }

h2 {  color: #3399ff;    }
h3 {  color: #3399ff;  	}
pre {  color: #FFF;    }
slide { background-color: #000000;}

ul{ color: #FFF;}
p{ color: #FFF;}
.title-slide hgroup h1 {color: #FFF;}

</style>


# Outline

## Outline {.flexbox .vcenter}


<div class="columns-2">

 <div class="valign">
  - Entstehung des GIS
  - Probleme
  - Ziele
  - Stand
 </div>

<div class="centered">
<img src="./Images/LeicaT1600.jpg" alt="Leica T1600.jpg" style="height:300px">
</div>

</div>

<footer class="source">Image: Leica T1600  |  © [Leica Geosystems]((http://www.leica-geosystems.com)) </footer>
</div>

# Entstehung

## 1994-1997: Neue topographische Karte | Dipl.-Ing. H. P. Birk


  - Topographische Geländeaufnahme: ca. 62.000 Messungen <!--(LEICA TC 1600, TC1610) -->
     - Geländemodells, abgeleitete Höhenlinien
     - Übername der 5 m-Höhenlinien aus den Plänen der Landesvermessung
  <br>
  <br>
  - Architekturpläne
     -  gescannt, georeferenziert, vektorisiert
     
## Input 2007-2011 | Survey und Hattuša's Buch

<div class="columns-2">

 <div class="centered">
 Hattuscha - Auf den Spuren der sagenhaften Hauptstadt der Hethiter
 <img src="./Images/Schachner2011.jpg" alt="Logo" style="height:320px">
 </div>

<br> <br> 
 
 <div class="centered">
Innenstadt Survey 2007-2009
 <img src="./Images/Survey.jpg" alt="Logo" style="height:300px">
 </div>


<footer class="source">Cover |  © Beck Verlag </footer>

</div>

## 2009-2011 | GRASS GIS Entwicklung

<div class="columns-2">

 <div class="centered">
 <img src="./Images/UlfRöttger.png" alt="Ulf Röttger" style="width:170px">
 </div>
 <div class="centered">
 <img src="./Images/grasslogo_vector_big.png" alt="Logo" style="width:170px">
 </div>


  
  <br> <p> tree </p> <br>
  |   |   |-- [&nbsp;33M]&nbsp;&nbsp;<b class="EXEC">luftbild_blur</b><br>
  |   |   |-- [&nbsp;22M]&nbsp;&nbsp;<b class="EXEC">luftphoto01</b><br>
	|   |   |-- [8.0M]&nbsp;&nbsp;<b class="EXEC">luftphoto01.r</b><br>
	|   |   |-- [&nbsp;22M]&nbsp;&nbsp;<b class="EXEC">luftphoto01.red</b><br>
	|   |   |-- [&nbsp;78M]&nbsp;&nbsp;<b class="EXEC">luftwaffe</b><br>
	|   |   |-- [&nbsp;72M]&nbsp;&nbsp;<b class="EXEC">luftwaffe_rec</b><br>
   <hr> <br> 	<p> 1692 directories, 10270 files, <br> totalling 25.2 GB	</p>
</div>

<footer class="source">[Ulf Röttger](http://mimr.io/#about), [GRASS GIS](http://grass.osgeo.org/download/logos/)  </footer>

</div>


## 2012-2014 | QGIS Entwicklung

<div class="columns-2">

 <div class="centered">
 <div class="centered">
 <img src="./Images/grasslogo_vector_big.png" alt="Logo" style="width:170px">
 </div>
 <img src="./Images/QGisLogo.png" alt="QGis Logo.png" style="width:170px">
 </div>

 

  <br> <p> tree </p>
  |   |   |-- [&nbsp;12M]&nbsp;&nbsp;<b class="EXEC">BKY_Mitt_mod.tif</b><br>
  |   |   |-- [1.7M]&nbsp;&nbsp;<b class="EXEC">BKY_Mitt.png</b><br>
  |   |   |-- [&nbsp;542]&nbsp;&nbsp;<b class="EXEC">BKY_Mitt.png.points</b><br>
  |   |   |-- [&nbsp;15M]&nbsp;&nbsp;<b class="EXEC">BKY_OberPlat_mod.tif</b><br>
  |   |   |-- [158K]&nbsp;&nbsp;<b class="EXEC">BKY_OberPlat.png</b><br>
  |   |   |-- [&nbsp;375]&nbsp;&nbsp;<b class="EXEC">BKY_OberPlat.png.points</b><br>
  <hr> <br> 	<p> 3852 directories, 23936 files , totalling 73.9 GB	</p>
</div>

<footer class="source">[QGis Logo](http://www.qgis.org) | CC BY-SA 3.0  </footer>

# Probleme

## Strukturell

  - Unübersichtlich Ordner-"Struktur"
  - Kein Standart in den Namen 
  - Keine Dokumentation
  - Keine Attribution 
  - Doppelungen
  - Größe des Projektes (80 GB), abhänging von der Software 'QGIS'
  - Keine Möglichkeit für Zusammenarbeit
  - GIS benutzt wie ein CAD Program


## Layer statt Attribut{.smaller}

<div class="columns-2">

|-- Haus1<br>
|   |-- Haus 1_Polygon.dbf <br>
|   |-- Haus 1_Polygon.prj <br>
|   |-- Haus 1_Polygon.qpj <br>
|   |-- Haus 1_Polygon.shp <br>
|   |-- Haus 1_Polygon.shx <br>
|   |-- Haus 1.qpj.dbf <br>
|   |-- Haus 1.qpj.prj <br>
|   |-- Haus 1.qpj.qpj <br>
|   |-- Haus 1.qpj.shp <br>
|   `-- Haus 1.qpj.shx <br>
....

<br><br><br>
|-- Haus51 <br>
|   |-- Haus 51.dbf <br>
|   |-- Haus 51_Polygon.dbf <br>
|   |-- Haus 51_Polygon.prj <br>
|   |-- Haus 51_Polygon.qpj <br>
|   |-- Haus 51_Polygon.shp <br>
|   |-- Haus 51_Polygon.shx <br>
|   |-- Haus 51.prj <br>
|   |-- Haus 51.qpj <br>
|   |-- Haus 51.shp <br>
|   `-- Haus 51.shx <br>
<br>

= 10 files (pro Haus) * 51 = 510 files <br>
-> 10 files + Attribut (Hausnummer)

## Attribut Tabelle

```{r Read DBF, echo=TRUE, warning=TRUE, include=FALSE}
library(foreign)
Qdm <- read.dbf("./Images/BackgroundStadtmauer.dbf", as.is=TRUE)
```

```{r, results='markup'}
head(Qdm )
```

# Ziele

## 'IANUS Konformität'

 <div class="valign">
  - Größe des Projektes reduzieren
  - Doppelungen löschen
  - Festlegung von Namen und Abkürzungen 
    - Normalisierung: BKY, NWO, ... (grep)
  - Trennen nach Rechte (DAI, Karte, 'Google')
  - Software unabhängig
  - Kooperationsfähig
 </div>

## Projekt- und Archivfähige Struktur

 <div class="valign">
  - Strukturietung nach Art
    - shapefiles
    - raster 
    
   - Nicht berücksichtig (jetzt)
    - geophysik 
    - 3d 
    

  </div>

# Implementierung
  
## Lösung: Gitlab

'Webbasierte Oberfläche für Softwareprojekte auf Basis der Versionsverwaltung git'
(wikipedia.de)

 <div class="centered">
 <img src="./Images/Gitlab-4-AccesControls.png" alt="Logo" style="width:800px">
 </div>

  
  <div class="centered">
  <a href="https://gist.dainst.org">www.gist.dainst.org</a>
  </div>

## Was ist 'Git'

  - Versionskontrollsysteme (VCS) protokollieren Änderungen an einer Datei oder einer Anzahl von Dateien über die Zeit hinweg
  - Unterstützung von verteilter Softwareentwicklung
  
  <div class="centered">
  <img src="./Images/Gitlab-2-Git-Distributed.png" alt="Logo" style="width:300px">
  </div>

## Warum Git

 <div class="centered">
 <img src="./Images/Gitlab-3-IntegrationManaerWorkflow.png" alt="Logo" style="width:600px">
 </div>

## GitLab

  <div class="centered">
  <a href="https://gist.dainst.org/bogazkoy/BoGIS">Visit www.gist.dainst.org/bogazkoy/BoGIS</a>
  </div>
  

# In Arbeit

## Automatisieren

  - Extract Daten von Ulf Röntger
  - Hook: Skripte ausführ wenn bestimmte, wichtige Ereignisse auftreten

# Danke

## Colophon


Presentation written in [Markdown](http://daringfireball.net/projects/markdown/) ([ioslides](http://rmarkdown.rstudio.com/ioslides_presentation_format.html))

Compiled into HTML5 using [RStudio](http://www.rstudio.com/ide/)

Source code hosting: https://github.com/nehemie/slideshow

ORCID: http://orcid.org/0000-0002-2898-6217

<footer class="source">This slide is a modulation of the colophon of [Ben Marwick's](http://orcid.org/0000-0001-7879-4531) [colophon on Reproducibility](http://benmarwick.github.io/UW-eScience-reproducibility-social-sciences/#/39)  |  B. Marwick CC BY 3.0 </footer>




<!--
---
nocite: 
...

## References {.smaller}
--> 
