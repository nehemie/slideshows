---
title: "Shaping a site into a Kingdom's Capital"
subtitle: "The case of Hattusa"
author: '[Néhémie Strupler](mailto:nehemie.strupler@etu.unistra.fr)'
date: "22-23/11/2016 | Seventh IFEA Archaeological Meetings"
output:
  ioslides_presentation:
    fig_height: 4
    fig_width: 7
    highlight: haddock
    logo: ./images/anamed_logo.png
    toc: yes
    transition: 0
    widescreen: yes
bibliography: bibliography.bib
---
<style>
body {  background-color: black; 
}

h2 {  color: #3399ff;    }
h3 {  color: #3399ff;  	}
slide { background-color: #000000;}

ul{ color: #FFF;}
p{ color: #FFF;}
.title-slide hgroup h1 {color: #FFF;}
</style>

# Intro

## From a city-state to a Kingdom's capital
 
<div class="centered">
<img src="./images/Barjamovic2012a-Map1.jpg" alt="Barjamovic 2012" style="height:460px">
</div>
<footer class="source">@Barjamovic2012a, Map 1 </footer>


## Ḫattuš (ca. 1750 BC) 
 
<div class="centered">
<img src="./images/Planning-MBA-UST.png" alt="Occupation around 1750 BC" style="height:460px">
</div>
<footer class="source">City of Ḫattuš around 1750 BC |  Strupler 2016</footer>



## Valorisation of heritage {.flexbox .vcenter}

P[iyusti] had [f]ortified Ḫattuša. So I (Anitta) left it alone. But
subsequently, when it became most acutely beset with famine(?), their goddess
Halmassuit gave it over (to me), and I took it at night by storm. In its place I
sowed cress. 

Whoever after me becomes king and resettles Hattusa, [let] the Stormgod of the 
Sky strike him.


## The destroy and curse it method...
 
<div class="centered">
<img src="./images/Max Klingensmith-FIRE--CCND.jpg" alt="Max Klingensmith, FIRE, 
CC ND (via Flickr.com)" style="height:460px">
</div>
<footer class="source">Fire | Max Klingensmith CC ND (via Flickr.com)</footer>


## One man's loss is another man's gain

<div class="centered">
<img src="./images/NWH.jpg" alt="Destruction layer from KNW" style="height:460px">
</div>
<footer class="source">Burned room | Strupler 2011, Boğayköy Expedition</footer>



## Elderly care 
 
<div class="centered">
<img src="./images/4-5-Bo57-KB4-12.jpg" alt="Canalisation on Karum Period"
style="height:460px">
</div>
<footer class="source">Excavation 1957 at Nordviertel | Boğayköy Expedition </footer>






## New city {.flexbox .vcenter}
 
<div class="centered">
<img src="./images/Begin-AerialView.jpg" alt="Arial View" style="height:460px">
</div>
<footer class="source">Aerial View of the city | Boğayköy Expedition</footer>

## New city {.flexbox .vcenter}
 
<div class="centered">
<img src="./images/Planning-LBA-UST-15.jpg" alt="15th century BC" style="height:460px">
</div>
<footer class="source">Plan of the Lower City during the 15th century BC | Strupler 2016</footer>

## New city {.flexbox .vcenter}
 
 
<div class="centered">
<img src="./images/Tempel1.jpg" alt="Tempel 1" style="height:460px">
</div>
<footer class="source">Street next to the Tempel 1 | Boğayköy Expedition</footer>








## Shaping a capital {.flexbox .vcenter}
 
<div class="centered">
<img src="./images/KNW-Bo2010.jpg" alt="KNW-Bo2010" style="height:460px">
</div>
<footer class="source">Excavation at Kesikkaya Nordwest in 2010 | Strupler, Boğayköy Expedition </footer>




## Shaping a capital {.flexbox .vcenter}
 
<div class="centered">
<img src="./images/MBA-KNO-3.jpg" alt="Fouilles KNW" style="height:460px">
</div>
<footer class="source">Excavation at Kesikkaya Nordwest in 2013 | Strupler, Boğayköy Expedition</footer>





## Discovery in 1834 by Charles Texier
 
<div class="centered">
<img src="./images/Texier.jpg" alt="Texier" style="height:460px">
</div>
<footer class="source">Tempel I | @Texier1839</footer>

## Discovery of Yazılıkaya
 
<div class="centered">
<img src="./images/Yazilikaya.jpg" alt="Texier" style="height:460px">
</div>
<footer class="source">Yazılıkaya | @Texier1839</footer>







## First Excavations (1906-1907) 
 
<div class="centered">
<img src="./images/1907-Kohl-Puchstein.jpg" alt="First Excavation" style="height:460px">
</div>
<footer class="source">Excavation 1907: Heinrich Kohl, Erich Puchstein and two
excavation workers | Boğayköy Expedition</footer>



##  First Excavations (1911-1912)
 
<div class="centered">
<img src="./images/1912-Winckler.jpg" alt="Excavation House and Winckler, 1912" style="height:460px">
</div>
<footer class="source">Excavation House and Winckler, 1912 | Boğayköy Expedition</footer>


## Second Excavations (1931-1938)
 
<div class="centered">
<img src="./images/BK-beginning-1931.jpg" alt="First Excavation" style="height:460px">
</div>
<footer class="source">Beginning of the second excavation Period, 5 Sept 1931 | Boğayköy Expedition</footer>



## Team in 1934
 
<div class="centered">
<img src="./images/DACH-1934.jpg" alt="First Excavation" style="height:460px">
</div>
<footer class="source">Team of 1934 | Boğayköy Expedition</footer>


## Preservation of culture heritage
 
<div class="centered">
<img src="./images/BK-Ia-Brunnenweg.jpg" alt="First Excavation" style="height:460px">
</div>
<footer class="source">Büyükkale Excavation (1950s)  | Boğayköy Expedition</footer>


## Earthfill
 
<div class="centered">
<img src="./images/BK-BoAr.jpg" alt="First Excavation" style="height:420px">
</div>
<footer class="source">Büyükkale Excavation (1960s)  | Boğayköy Expedition</footer>


## Earthfill
 
<div class="centered">
<img src="./images/Begin-AerialView.jpg" alt="Arial View" style="height:460px">
</div>
<footer class="source">Aerial View of the city | Boğayköy Expedition</footer>




## Restauration of buildings (1960s owards)
 
<div class="centered">
<img src="./images/tempel.jpg" alt="Tempel" style="height:460px">
</div>
<footer class="source">Tempel 30</footer>



## Restauration of Yerkapı
 
<div class="centered">
<img src="./images/Neve-Yerkapi-1.jpg" alt="Yerkapı" style="height:460px">
</div>
<footer class="source">Yerkapı</footer>



## Restauration of Yerkapı
 
<div class="centered">
<img src="./images/Neve-Yerkapi-2.jpg" alt="Yerkapı" style="height:460px">
</div>
<footer class="source">Yerkapı</footer>


## Nifi Amca
 
<div class="centered">
<img src="./images/Neve_in_the70s.jpg" alt="Yerkapı" style="height:460px">
</div>
<footer class="source">Yerkapı</footer>




## Unesco World Heritage List {.flexbox .vcenter}
 
<div class="centered">
<img src="./images/unesco.jpg" alt="Tempel" style="height:460px">
</div>
<footer class="source">Unesco 1986</footer>




##  Kammer 2 (discovery 1988, anastylosis 1990s)
<div class="columns-2"> 
<div class="centered">
<img src="./images/kammer2a.jpg" alt="Kammer 2" style="height:260px">
</div>
<footer class="source">Kammer 2</footer>

 
<div class="centered">
<img src="./images/kammer2c.jpg" alt="Kammer 2" style="height:260px">
<img src="./images/kammer2b.jpg" alt="Kammer 2" style="height:260px">
</div>
<footer class="source">Kammer 2</footer>
</div> 







## Yazılıkaya Case 

 <div class="centered">
<img src="./images/Yazilikaya-open.jpg" alt="Yazilikaya" style="height:460px">
</div>
<footer class="source">View of Yazılıkaya </footer>



##  Yazılıkaya Case

<div class="columns-2"> 
<div class="centered">
<img src="./images/Yazilikaya--Masson.jpg" alt="Cover" style="height:460px">
</div>

 
<div class="centered">
<img src="./images/Yazilikaya--Guterbock.jpg" alt="Cover" style="height:460px">
</div>
<footer class="source">Emilia Masson, Le Panthéon de Yazılıkaya 1981 et Hans G. Güterbock, Les hiéroglyphes de Yazılıkaya, 1982</footer>

</div> 




## Yazılıkaya Case {.flexbox .vcenter}

 <div class="centered">
<img src="./images/Yazilikaya--panegyricsOfGranovetter--CCBYSA.jpg" alt="Yazılıkaya" style="height:460px">
</div>
<footer class="source">Yazılıkaya | Panegyrics of Granovetter-- CC BY-SA (via flickr.com) </footer>









## Sphinx Case {.flexbox .vcenter}
 
<div class="centered">
<img src="./images/Sphinxtor1907.jpg" alt="Sphinx 1907" style="height:460px">
</div>
<footer class="source">Sphinx 1907. "Makridi removed the sphinxes for
conservation"</footer>

<!-- 
## Sphinx Case {.flexbox .vcenter}
 
<div class="centered">
<img src="./images/2012-05-19-The Economist.jpg" alt="2012-05-19-The Economist" style="height:460px">
</div>
<footer class="source">2012-05-19-The Economist</footer>
-->

## Sphinx Case 
 
<div class="columns-2"> 
<div class="centered">
<img src="./images/Parzinger2011a--.jpg" alt="Parzinger2011" style="height:360px">
</div>


<div class="centered">
<img src="./images/Parzinger2012a--WirVerschweigenNichts.jpg" alt="Parzinger2012" 
style="height:460px">
</div>
</div>

## Sphinx Case {.flexbox .vcenter}
 
<div class="centered">
<img src="./images/Sphinx2012.jpg" alt="Sphinx 2012" style="height:460px">
</div>
<footer class="source">Sphinx 2012</footer>



# Conlusion

<!-- 

-->


# Thank you 

## Colophon


Presentation written in [Markdown](http://daringfireball.net/projects/markdown/) 

Compiled into HTML5 using [RStudio](http://www.rstudio.com/ide/) and [ioslides](http://rmarkdown.rstudio.com/ioslides_presentation_format.html)

Source code hosting: https://gitlab.com/nehemie/slideshow

ORCID: [http://orcid.org/0000-0002-2898-6217](ORCID: http://orcid.org/0000-0002-2898-6217)

<footer class="source">This slide is a modulation of the colophon of [Ben Marwick's](http://orcid.org/0000-0001-7879-4531) [colophon on Reproducibility](http://benmarwick.github.io/UW-eScience-reproducibility-social-sciences/#/39)  |  B. Marwick CC BY 3.0 </footer>


### Licensing:

Presentation: [CC BY 4.0 ](http://creativecommons.org/licenses/by/4.0/)

Source code: [MIT](http://opensource.org/licenses/MIT) 

<!--
---
nocite: | 
 @Fitzpatrick2011a  @Kelty2008, @Kansa2014a, @Kansa2014b, @Lessig2001a, @Sonke2014 
...
-->

## References {.smaller}
