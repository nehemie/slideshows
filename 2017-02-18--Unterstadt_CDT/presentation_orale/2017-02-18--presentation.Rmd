---
title: "La ville basse de Boğazköy \\newline au IIe millénaire av. J.-C." 
subtitle: "Une étude de l'organisation urbaine de la cité-État et de sa restructuration en capitale du royaume hittite"
author: "Néhémie Strupler"
#institute: "Université de Strasbourg & Westfälische Wilhelms Universität Münster"
#date: "Thèse de doctorat soutenue le 16 septembre 2016"
output: 
  beamer_presentation:
    slide_level: 2
    theme: "Pittsburgh"
    colortheme: "seahorse"
    fonttheme: "structurebold"
    highlight:  zenburn
    keep_tex: true
    includes:
      in_header: "2017-02-18--presentation-header.tex"
    fig_caption: false
hyperref:
  urlcolor: red
  linkcolor: red
---

##  Deux systèmes politiques

\begin{columns}
 \begin{column}{.5\linewidth}

  \begin{figure}
  Bronze moyen
  
  \includegraphics[width=1\linewidth]{images/Barjamovic2012a-Map1}

  \vfill
  {\tiny Organisation en cités-États (Barjamovic et al. 2012)}
  \end{figure}

 \end{column}
 \begin{column}{.6\linewidth}

  \begin{figure}
  Bronze récent
  
  \fbox{\includegraphics[width=1\linewidth]{images/Glatz2005b}}
  
  \vfill
  {\tiny Relations de dominance dans l'Empire hittite (Glatz 2005).}
  \end{figure}

 \end{column}
\end{columns}

\vspace{1cm}
 \center -> Quels effets sur la population ? 


## Flash-back
 
  \begin{figure}
  \fbox{\includegraphics[width=0.8\linewidth]{images/new/old_presentation}}
  \end{figure}

## Flash-back
 
  \begin{figure}
  \fbox{\includegraphics[width=0.8\linewidth]{images/new/old_presentation_fragestellung}}
  \end{figure}

# Foule et intégration à Boğazköy 

## Dénombrer la foule

  \begin{figure}
  \includegraphics[width=1\linewidth]{images/new/Istanbul}
  \end{figure}

##  La 'masse'

### 6.2.1 La taille de Boğazköy

D'une manière générale, les études paléodémographiques des villes
de l'Âge du Bronze en sont encore à leurs débuts et on ne peut que regretter
l'absence de discussion à ce sujet. Même si, nous le verrons, chaque modèle
repose obligatoirement sur de  nombreuses prémisses, cette absence de discussion
montre que l'histoire des « grands » occulte l'étude de la « masse » --  la
population -- qu'il est difficile de différencier pour les archéologues. Alors
que certaines régions ou périodes ont reçu nettement un peu plus d'attention pour
évaluer le nombre d'habitant.e.s d'un site, les essais sont rares pour
l'Anatolie centrale.


## 'Big picture' problem

  \begin{figure}
  \includegraphics[width=0.9\linewidth]{images/new/Palmisano_results}
  \end{figure}

  \tiny Palmisano 2015, \emph{Landscapes of interaction and conflict in the Middle Bronze Age}, Journal of Archaeological Science Reports 3, 2015
  (65 ha, Kültepe 50 ha)

# Présentation du site

## Plan général

  \begin{figure}
  \fbox{\includegraphics[width=0.55\linewidth]{images/supp/Stadt-MauerHydro}}
  \end{figure}




## Plan de la ville basse

  \begin{figure}
  \fbox{\includegraphics[width=0.45\linewidth]{images/supp/Stadt-Unterstadt-Ovalen-Archi}}
  \end{figure}


## Vue sur la ville basse

 \begin{figure}
  \includegraphics[width=0.8\linewidth]{images/Photo-Ust}
  \end{figure}
  

\vspace{-0.4cm}
  \begin{figure}
  \includegraphics[width=0.5\linewidth]{images/Tab-Chrono-Debut-1}
  \end{figure}


\Fontvi
```{r, echo=FALSE, eval=FALSE}
table <- read.csv("images/Periods.tsv", sep = "\t", check.names = FALSE)
knitr::kable(table, include.colnames = FALSE,
               include.rownames = FALSE)

```

# Bronze Moyen

## Occupation du Bronze moyen

  \begin{figure}
  \includegraphics[width=0.5\linewidth]{images/new/Planning-MBA-UST}
  \includegraphics[width=0.5\linewidth]{images/new/Planning-MBA-WTer}
  \end{figure}


## Détail de l'ensemble le mieux conservé

  \begin{figure}
  \includegraphics[width=0.8\linewidth]{images/all/Dwelling-MBA-WTer}
  \end{figure}


```{r, echo=FALSE, eval=TRUE}
table <- read.csv("images/new/mba_area_nov.tsv", sep = "\t", check.names = FALSE)
#knitr::kable(table, include.colnames = FALSE,
#               include.rownames = FALSE, pad = 0)
```
```{r, echo=FALSE, eval=TRUE, results='asis'}
print(xtable::xtable(table[,c(1, 2 ,4:5)]), include.colnames=TRUE, include.rownames=FALSE,,
      comment=FALSE, size="\\small"
      )
```

## Taille des maisons au Bronze moyen

  \begin{figure}
  \includegraphics[width=0.8\linewidth]{images/all/Dwelling-Houses-MBA-Bo-Kul}
  \end{figure}

## Taille de l'occupation du Bronze moyen

  \begin{figure}
  \includegraphics[width=0.60\linewidth]{images/new/Dwelling-MBA-Zone-Bo}
  \end{figure}
  Total : \SI{253521}{\square\meter}, ou \SI{25,34}{\hectare}, ou encore \SI{0,253}{\square\kilo\meter}

## Dénombrement


\small Surface totale de \SI{0,25}{\square\kilo\meter} / \SI{175}{\square\meter}  (emprise au sol moyenne des maisons de type "Boğazköy")
* \SI{65}{\percent} = \num{950} maisons à Boğazköy  

\vspace{1cm}

$\frac{253521\times0,65}{175}$ = 941.65 
\vspace{1cm}

Si on attribue la valeur de 8 personnes par maisonnée (famille de cinq membres
et trois domestiques) =  \num{7500} personnes à Boğazköy

\vspace{1cm}

$\frac{253521\times0,65}{175}\times8$ = 7533,195 


## Taille de l'occupation du Bronze moyen

  \begin{figure}
  \includegraphics[width=0.50\linewidth]{images/new/Dwelling-MBA-Zone-Bo}
  \end{figure}
  Postule 65\%, 50\% et 20\% -> 670 maison -> 5300 personnes

## Bilan pour le Bronze Moyen

  Entre 2000 et 4000 habitant.e.s

  

# Bronze récent

## Situation générale
  \begin{figure}
  \includegraphics[width=0.5\linewidth]{images/new/Planning-LBA-UST-16}
  \includegraphics[width=0.5\linewidth]{images/new/Planning-LBA-WTER-16}
  \end{figure}


## Détail de l'ensemble urbain le mieux conservé

  \begin{figure}
  \includegraphics[width=1\linewidth]{images/Dwelling-LBA-WTer}
  \end{figure}


## Taille des bâtiments

  \begin{figure}
  \includegraphics[width=1\linewidth]{images/new/qdm-LBA-Building}
  \end{figure}



## Zones d'occupation au Bronze récent

  \begin{figure}
  \includegraphics[width=0.45\linewidth]{images/Dwelling-LBA-Zone-Bo}
  \end{figure}

\footnotesize Somme totale des surfaces équivaut à \SI{379712}{\square\meter}, 
\SI{37,950}{\hectare}, ou \SI{0,379}{\square\kilo\meter}. La surface
totale encadrée par le rempart est de \SI{76,25}{\hectare}

\footnotesize Estimation :  8000 habitant.e.s ?


## Les activités

\begin{columns}
 \begin{column}{.65\linewidth}

  \vspace{-0.3cm}
  \begin{figure}
  \includegraphics[width=0.9\linewidth]{images/GIS-prodobjet-0}
  \end{figure}

 \end{column}
 \begin{column}{.40\linewidth}

  \begin{figure}
  \includegraphics[width=1\linewidth]{images/GIS-prodobjet-2}
  
  \vspace{1.4cm}
  
  \includegraphics[width=1\linewidth]{images/GIS-prodobjet-1}
  \end{figure}

 \end{column}
\end{columns}

\center \small -> Activités liées au petit artisanat et administration

## Répartitions outils d'administration : tablettes et cachets

  \begin{figure}
  \includegraphics[width=0.5\linewidth]{images/supp/GIS-Tablette-Dens}
  \includegraphics[width=0.5\linewidth]{images/supp/GIS-seals-density}
  \end{figure}

# Prospections autour de la ville

## Près de Yazılıkaya

![](images/supp/Schachner2008a--Bogazkoy_2007-ProspectionBaskent)

\tiny Schachner 2007

## Autour de Yazılıkaya

![](images/supp/Schachner2008a--Bogazkoy_2007-ProspectionYazilikaya)

\tiny Schachner 2007

## A l'ouest du site 

![](images/supp/Schachner2013a--Bo_2012-ProspectionOuest)

\tiny Schachner 2012

## Ahmet Can Tarlası (nord)

![](images/supp/Schachner2014a--Bo_2013--AhmetCan.Tarlasi)

\tiny Schachner 2014

## Prospection au nord

![](images/supp/Schachner2014a--Bo_2013--Nord)

\tiny Schachner 2014

<!--
## Bronze récent

  \begin{figure}
  \includegraphics[width=0.8\linewidth]{images/supp/Dwelling-Bo-LBA-Emrpise_Surface}
  \end{figure}

-->

 

# Conclusion

## Conclusion

 >- La masse de la foule : rarement estimée 
 
 >- Ne se laisse pas réduire à des règles toutes faites   
 
 >- Regard particulier pour chaque cas permet d'en approcher la valeur
 

## Merci pour votre attention !

  \begin{figure}
  \includegraphics[width=1\linewidth]{images/new/Istanbul}
  \end{figure}
