\documentclass[12pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[a4paper]{geometry}
\usepackage{graphicx}

\pagestyle{empty}
\geometry{top=2cm, bottom=3cm, left=2cm, right=2cm}


\parindent=0pt \parskip=0pt

\begin{document}

\centerline{\bf R-chaeology}
\centerline{\bf Analyse et datation d'artefacts archéologiques : R et les cachets circulaires hittites}
\vspace{12pt}

\centerline{ {\bf N.~Strupler} $^{\rm a}$ $^{\rm b}$ }

\vspace{12pt}

\centerline{$^{\rm a}$
UMR 7044  \guillemotleft{} Archéologie et Histoire ancienne : Méditerranée - Europe  \guillemotright{} (ARCHIMÈDE)}
\centerline{Université de Strasbourg}
\centerline{5 allée du Gal Rouvillois, 67083 Strasbourg}


\vspace{12pt}

\centerline{$^{\rm b}$Institut f\"ur Altorientalische Philologie und Vorderasiatische Altertumskunde }
\centerline{Westf\"alische Wilhelms-Universit\"at M\"unster}
\centerline{Rosenstr. 9, 48143 Münster, Allemagne}
\vspace{12pt}
\centerline{nehemie.strupler@etu.unistra.fr}
\vspace{24pt}

{\bf Mots clefs} : statistique, chronologie, archéologie

\vspace{24pt}

Dans cette présentation nous montrerons comment les analyses statistiques
permettent aux sciences historiques, notamment à l'archéologie, de renouveler
des questionnements à travers une étude de cas.
Les outils statistiques sont encore peu employés  par les archéologues et les
philologues : des corpus immenses existent mais ne donnent généralement lieu à
aucune autre étude statistique qu'un simple dénombrement. Nous traiterons des
cachets circulaires hittites et nous regarderons comment une analyse factorielle
permet d'affiner la typologie et la datation d'artefacts. Une fois l'évolution
des objets mieux définie il est possible d'inférer sur la signification des
changements au sein de la civilisation. Après une présentation du corpus et de
la méthode d'investigation, les résultats illustreront des perspectives offertes
par R aux archéologues et aux chercheurs en sciences historiques.


\vspace{24pt}


Utilisés depuis des millénaires, les cachets de pierre ou de métal gravés en
creux ou en relief d'initiales, de titres ou d'emblèmes servent à sceller des
objets pour en attester l'authenticité et en contrôler leur ouverture. Les
cachets et leurs empreintes témoignent des pratiques aussi bien administratives
que artistiques de la civilisation qui les a produits. Le corpus des cachets
hittites offre l'avantage d'être assez large et homogène pour être étudié
statistiquement. 

De la civilisation hittite (Anatolie centrale, ca 1650--1200 av. n.è.)  nous
connaissons un corpus important d'empreintes de cachets circulaires;  plus de
3000 ont été découvertes dans la capitale Hattu\v{s}a, aujourd'hui à environ 150
km à l'est d'Ankara [1--3, 5--6]. Ces cachets appartiennent aux rois, aux reines
ou à certains fonctionnaires. Au sein des cette production, on connait bien
l'évolution des sceaux royaux dont l'ordonnance chronologique est assurée. En
revanche, l'évolution des sceaux des fonctionnaires est moins bien connue.

\begin{figure}[h]
\centering
\includegraphics[width=0.7\textwidth]{./Herborrdt2005-172-178}
\caption{Deux empreintes de cachets circulaires hittites (ca.
         14\textsuperscript{ème} siècle av. n.è.) D'après [5] Cat. 172 et 178.}
\label{UmarmungMuwatali}
\end{figure}

Tous les cachets circulaires ont été réalisés selon le même modèle avec une
plage centrale et des bordures circulaires (fig. \ref{UmarmungMuwatali}). La
plage centrale se compose généralement d'une inscription en louvite
hiéroglyphique mentionnant le titre et le nom du propriétaire  accompagnée de
divers motifs. Les bordures circulaires peuvent être garnies d'une légende en
akkadien, c'est le cas pour les cachets royaux, de hiéroglyphes, de
représentations humaines ou bien de motifs décoratifs. Parmi certaines variables
retenues dans cette étude, le motif central, la taille de la plage centrale, des
bordures circulaires, la forme du cachet, les hiéroglyphes, les éléments
décoratifs récurrents (triangles, cercles, motifs trilobés, coniques ou encore
aigles héraldiques bicéphales) permettent d'obtenir des données quantitatives et
qualitatives. 

\`{A} l'aide d'une analyse factorielle des données mixtes et du package
FactoMineR [7, 8] nous étudierons la variabilité des cachets cylindriques. Tout
d'abord, avec une analyse en composante principale des données métriques, il est
possible de distinguer différents groupes. Les hiéroglyphes seront étudiés grâce
à une analyse factorielle des correspondances. Les artefacts bien datés à
travers leur contexte archéologique ou philologique serviront de piliers pour
transcrire la variabilité en une évolution chronologique. Finalement nous
soulignerons les apports de l'analyse factorielle et son accessibilité grâce à R
: la corrélation entre la datation et les données métriques des cachets, le
titre, la représentation iconographique et les éléments décoratifs offrent une
nouveau regard pour interpréter les cachets dans leur contexte historique. 

\vspace{12pt}
{\bf R\'ef\'erences}

[1] Boehmer, R. M. ;  H. G. Güterbock (1987). \textit{Glyptik aus dem
Stadtgebiet von Bo\u{g}azköy}, Gebr. Mann, Berlin

[2] Güterbock, H. G. (1940). \textit{Die Königssiegel der Grabungen bis 1938},
Weidner, Berlin

[3] Güterbock, H. G. (1942). \textit{Die Königssiegel von 1939 und die übrigen
Hieroglyphensiegel}, Weidner, Berlin

[4] Hawkins, J. D. (2000). \textit{Corpus of hieroglyphic Luwian inscriptions},
de Gruyter, Berlin

[5] Herbordt, S. (2005). \textit{Prinzen- und Beamtensiegel der hethitischen
Grossreichszeit auf Tonbullen aus dem Ni\c{s}antepe-Archiv in Hattu\v{s}a}, von
Zabern, Mayence

[6] Herbordt, S. ; D. Bawanypeck, ; J. D. Hawkins (2011). \textit{Die Siegel der
Grosskönige und Grossköniginnen auf Tonbullen aus dem Ni\c{s}antepe-Archiv in
Hattu\v{s}a}, von Zabern, Mayence

[7] Husson, F. ; J. Josse ; S. Lê ;  J. Mazet (2013). \textit{FactoMineR :
Multivariate Exploratory Data Analysis and Data Mining with R}. R package
version 1.23

[8]  Husson, F. ; S. Lê ; J. Pagès (2011). \textit{Exploratory Multivariate
Analysis by Example Using R}, CRC Press, Boca Raton

[9] Laroche, E. (1960). \textit{Les hiéroglyphes hittites}, Éditions du CNRS,
Paris


\end{document}
