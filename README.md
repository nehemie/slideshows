# Slideshow

This folder contains slides and notes from some of my talks.

Licences: 
  - Textes: CC-BY  4.0 year: 2015, author: Néhémie Strupler
  - Images: CC-BY 4.0 year: 2015, author: Néhémie Strupler (Indicated otherwise if I am not the copyright holder) 
  - Code: MIT, year: 2015, copyright holder: Néhémie Strupler
  - Data: CC0 1.0


Thank you for your feedback!