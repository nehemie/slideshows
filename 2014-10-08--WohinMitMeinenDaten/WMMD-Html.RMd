---
title: "Raw Data and Data Analysis.<br> An improvement to archiving?"
author: "[Néhémie Strupler](mailto:nehemie.strupler@unistra.fr) <br>
[Université de Strasbourg](http://www.unistra.fr), 
[Westfälische Wihlems Universität Münster](http://www.uni-muenster.de/),
[DAI](http://www.dainst.org)"
date: "[Wohin mit meinen Daten?](http://www.ianus-fdz.de/projects/veranstaltungen/wiki/Wohin_mit_meinen_Daten)  | Deutscher Archäologiekongress,  Berlin, 8th October 2014 "
output:
  ioslides_presentation:
    fig_height: 4
    fig_width: 4
    highlight: haddock
    incremental: yes
    toc: yes
    transition: 0
    widescreen: yes
    keep_md: yes
    logo: ./images/Logo-DAI-UDS-WWU-White.png
hyperref:
  urlcolor: red
  linkcolor: red
bibliography: WMMD-Bib.bib
---

<style>

body {  background-color: black; 
}
aside.gdbar {
 background: -webkit-gradient(linear, 0% 50%, 100% 50%, color-stop(0%, #00cd00), color-stop(100%, #00cd00)) no-repeat;
  background: -webkit-linear-gradient(left, #00cd00, #00cd00) no-repeat;
  background: -moz-linear-gradient(left, #00cd00, #00cd00) no-repeat;
  background: -o-linear-gradient(left, #00cd00, #00cd00) no-repeat;
  background: linear-gradient(left, #00cd00, #00cd00) no-repeat;
}

h2 {  color: #3399ff;    }
h3 {  color: #3399ff;    }
slide { background-color: #000000;}

ul{ color: #FFF;}
ol{ color: #FFF;}
p{ color: #FFF;}
.title-slide hgroup h1 {color: #FFF;}

</style>

# Introduction


## Archaeology has an ethic of preservation

<div class="centered">
<img src="./images/Tagebuch1953.jpg" alt="Records of small finds, Boğazköy Expedition, 1953" style="height:420px">
</div>
<footer class="source">Image: Records of small finds, Boğazköy Expedition, 1953  |  © Boğazköy Expedition, [Deutsches Archäologisches Institut](http://www.dainst.org/)</footer>


## Huge amount of data
<div class="centered">
<img src="./images/Wordcloud-BeschreibungQuart.jpg" alt="Wordcloud einer Variable der Pergamon Datenbank" style="height:440px">
</div>
<footer class="source"> Wordcloud of a variable from the Peragmon Datenbank | © Grabung Pergamon, [Deutsches Archäologisches Institut](http://www.dainst.org)</footer>

 
## Why archiving and releasing data?{.build}

@Wallis2013 :1

### Release based on the "assumption that those data are useful to others" 
<br>
 
  - Understanding, control, verification: **reproducibility**
  - New research questions
  - New field of study: **Advancing research** and **innovations**
  - Maintain attractivness 


## Outline of the presentation

  1) Nature of raw data 
  2) Raw data in an archaeological project
  3) Statistic and archive
  








# Raw data


## Being in or nearly in the natural state

<div class="centered">
<img src="./images/mnist-train6.jpg" alt="Records of small finds, Boğazköy Expedition, 1953" style="height:440px">
</div>
<footer class="source">MNIST Handwritten Digits |  © Sam Roweis, Vision, Learning and Graphics Group, NYU, [www.cs.nyu.edu/~roweis](http://www.cs.nyu.edu/~roweis/data/mnist_train6.jpg)</footer>



## Collector and processor 

<div class="centered">

"The challenge for most researchers who collect and analyze data is to extract useful information from the raw data they start with." 


[@McIver2008]
</div>


## Why archive and publish data?

### Publishing of raw data is:

  - **Not rewarded**: "individual imperatives for career self-interest, advancing the field, and receiving credit are often more powerful motivators in publishing decisions than the technological affordances of new media."

### Archiving of raw data is:

 - **Not rewarded**: "Stand-alone cataloging or curating, protocols are not weighted as heavily as peer-reviewed “interpretive” work"
 - **Time consuming**: "Most scholars [...] are plagued with a lack of time; are in need of more, not fewer, filters and curated sources;" 

Citations: @Harley2013
 
## The sociotechnical system concept
<div class="centered">
<img src="./images/Hughes1991-Fig5.jpg" alt="Sociotechnical system" style="height:440px">
</div>
<footer class="source"> Sociotechnical system |  @Hughes1991 :23</footer> 

   
## The sociotechnical system concept | @Pfaffenberger1992, 498 and 506

 - "those who seek to develop new technologies must concern themselves not only with techniques and artifacts; they must also engineer the social, economic, legal, scientific, and political context of the technology. A successful technological innovation occurs only when all the elements of the system, the social as well as the technological, have been modified so that they work together effectively" . 
  
 - "To construct a sociotechnical system is not merely to engage in some creative or productive activity. It is to bring to life a deeply desired vision of social life"


# Archaeological raw data

## The Publishing and Pushing Model | @Kansa2014{.smaller}

<div class="columns-2">

  - Step 1: Solicitation and metadata documentation
  - Step 2: Review, decoding and editing of data
  - Step 3: Annotation and alignment
  - Step 4: Contributor review, peer review, and analysis
  - Step 5: Publication, indexing and archiving
  
<div class="centered">
<img src="./images/KansaWorkflow-Inv.png" alt="Data publication workflow" style="height:260px">
</div>
<footer class="source"> Data publication workflow |  @Kansa2014 :60 </footer> 

</div>

## Data Editors

  Work:
  
   - create supplemental documentation
   - make annotations
   - decodes, control the data-set
   - complete and correct the documentation
  
  Requirement:
  
  - domain knowledge 
  - management knowledge
  



## | The | exquisite|corpse | shall drink | the | new | wine

Predetermined patterns:

 - "The | **adjective** | **noun** | **verb** | the | **adjective** | **noun**"
 - "The |   exquisite | corpse | shall drink | the | new | wine"
 

## |The | exquisite|corpse | shall drink | the | new | wine

<div class="centered">
<img src="./images/CadavresExquisEcrits.jpg" alt="Cadavres exquis écrits" style="height:440px">
</div>
<footer class="source"> Cadavres exquis écrits, Victor Brauner, André Breton, Jacques Hérold, Elsie Houston, Benjamin Péret, Yves Tanguy,  21 janvier 1934 <br> © [http://www.andrebreton.fr](http://www.andrebreton.fr/fr/item/?GCOI=56600100078640) </footer> 


 
## |The | exquisite|corpse | shall drink | the | new | wine {.build}

<div class="columns-2">

<div class="centered">
<img src="./images/CadavreExquisEncre.jpg" alt="Cadavre exquis à l'encre" style="height:440px">
</div>
<footer class="source"> Cadavre exquis, André Breton, Max Morise, Emmanuel Rudzitsky dit Man Ray, Yves Tanguy, 17 mai 1927 <br> © [http://www.andrebreton.fr](http://www.andrebreton.fr/fr/item/?GCOI=56600100341770) </footer> 


</div>

## |The | exquisite|corpse | shall drink | the | new | wine

<div class="columns-2">

<div class="centered">
<img src="./images/CadavreExquisEncre.jpg" alt="Cadavre exquis à l'encre" style="height:440px">
</div>
<footer class="source"> Cadavre exquis, André Breton, Max Morise, Emmanuel Rudzitsky dit Man Ray, Yves Tanguy, 17 mai 1927 <br> © [http://www.andrebreton.fr](http://www.andrebreton.fr/fr/item/?GCOI=56600100341770) </footer> 

 "our standard, taken-for-granted categorizations of material remains served to reproduce an existing way of thinking about the 
past and exclude other possibilities" @Pollock2010 : 42

</div>


# Raw data

## Open data "in natural state"?

<div class="centered">
<img src="./images/LinkedARC.jpg" alt="Open Data example" style="height:440px">
</div>
<footer class="source"> Linked Open Data <br> </footer> 

## Open data "in the natural state"

<div class="centered">
<img src="./images/Raw-DataToFirstAnalyse-Inv.png" alt="Raw-DataToFirstAnalyse" style="height:300px">
</div>
<footer class="source"> Raw-Data must be processed | Néhémie Strupler 2014 - CC BY 4.0<br> </footer> 


## Open data 

<div class="centered">
<img src="./images/RPie-Inv.png" alt="Rpie" style="height:500px">
</div>
<footer class="source">Pie chart of Hittite legends from seals | Néhémie Strupler 2014 - CC BY 4.0<br> </footer> 


## Script (R languange)
Deconcatenate the Data
```{r, eval=FALSE}
for (ind in 1:length(allValues)) {
  contTitre.H[ind,] <- sapply(uniqueVal, function(aVal)
    sum(allValues[[ind]]==aVal))
    for (ind2 in 1:length(uniqueVal)) {
      if (contTitre.H[ind,ind2]>1){
      contTitre.H[ind,ind2]= 1 } } }
```
Sum the Data
```{r, eval=FALSE}
uniqueVal<-gsub(",", "", uniqueVal)
for (ind in 1:length(uniqueVal)){
  u[,ind] <- paste(uniqueVal[ind], ".TH", sep="")}
```
Plot the Data
```{r, eval=FALSE}
barplot(Thiero[1:22], horiz=TRUE, col=rainbow(32))
```



## Open data cleaned 

<div class="centered">
<img src="./images/RBarGraph-Inv.png" alt="Bar Graph" style="height:440px">
</div>
<footer class="source">Bar chart of most common Hittite hieroglyphes from seals | Néhémie Strupler 2014 - CC BY 4.0<br> </footer> 


## Statistic as a "bottom up" process

<div class="centered">
<img src="./images/DataEditor-Inv.png" alt="Data Managment" style="height:440px">
</div>
<footer class="source">Data Managment | Néhémie Strupler 2014 - CC BY 4.0<br> </footer> 



# Conclusion


# References


<!--
## References

\begin{thebibliography}{17}

\bibitem{Arbuckle2014}
Arbuckle BS, Kansa SW, Kansa E, Orton D, Çakırlar C, et al. (2014) Data Sharing Reveals Complexity in the Westward Spread of Domestic Animals across Neolithic Turkey \emph{PLoS ONE}, 9(6): e99845. \href{http://dx.doi.org/10.1371/journal.pone.0099845}{doi: 10.1371/journal.pone.0099845}


\bibitem{Gell1992}
  Gell A (1992) The Technology of Enchantment and the Enchantment of Technology, in Coote J, Shelton A. (Eds.), \emph{Anthropology, art, and aesthetics}, Oxford, 44--60.


\bibitem{Harley2013}
  Harley D. (2013) Scholarly Communication: Cultural Context, Evolving Models
  \emph{Science}, 342 (6154), 80-82. \href{http://dx.doi.org/10.1126/science.1243622}{doi:10.1126/science.1243622}

\end{thebibliography}
## References
\begin{thebibliography}{17}


\bibitem{Hughes1991}
  Hughes TP (1991) From Deterministic Dynamos to Seamless-Web Systems, in: Sladovich, HE (Ed.) \emph{Engineering as a Social Enterprise}. Washington DC, 7--25. \url{http://www.nap.edu/openbook.php?record_id=1829&page=7}


\bibitem{Kansa2014}
Kansa EC, Kans SW, Arbuckle B (2014) Publishing and Pushing: Mixing Models for Communicating Research Data in Archaeology, \emph{International Journal of Digital Curation}, 9.1, 57-70. \href{http://dx.doi.org/10.2218/ijdc.v9i1.301}{doi: 10.2218/ijdc.v9i1.301}


\bibitem{McIver2008}
  McIver JP (2008) Raw Data in: Lavrakas (PJ) \emph{Encyclopedia of Survey Research Methods}.

\end{thebibliography}
## References
\begin{thebibliography}{17}



\bibitem{Pfaffenberger1992}
  Pfaffenberger B (1992) Social Anthropology of Technology \emph{Annual Review of Anthropology}, 1992, 21, 491-516. \href{http://dx.doi.org/10.1146/annurev.an.21.100192.002423}{doi: 10.1146/annurev.an.21.100192.002423}

\bibitem{Pollock2010}
 Pollock S, Bernbeck R (2010) An Archaeology of Categorization and Categories in Archaeology, \emph{Paléorient} 36, 37-47.


\bibitem{Wallis2013}
   Wallis JC, Rolando E, Borgman CL (2013) If We Share Data, Will Anyone Use Them? Data Sharing and Reuse in the Long Tail of Science and Technology. \emph{PLoS ONE} 8(7): e67332. \href{http://dx.doi.org/10.1371/journal.pone.0067332}{doi: 10.1371/journal.pone.0067332}

\end{thebibliography}
-->

## About this slideshow

## Colophon



Presentation written in [Markdown](http://daringfireball.net/projects/markdown/) ([ioslides](http://rmarkdown.rstudio.com/ioslides_presentation_format.html))

Compiled into HTML5 using [RStudio](http://www.rstudio.com/ide/)

Source code hosting: [https://github.com/nehemie/slideshow/](https://github.com/nehemie/slideshow/2014-10-08--WohinMitMeinenDaten)

ORCID: [http://orcid.org/0000-0002-2898-6217](ORCID: http://orcid.org/0000-0002-2898-6217)


<footer class="source">This slide is a modulation of the colophon of [Ben Marwick's](http://orcid.org/0000-0001-7879-4531) [colophon on Reproducibility](http://benmarwick.github.io/UW-eScience-reproducibility-social-sciences/#/39)  |  B. Marwick CC BY 3.0 </footer>

<div class="columns-2">

### Licensing:

Presentation: [CC BY 4.0 ](http://creativecommons.org/licenses/by/4.0/)

Source code: [MIT](http://opensource.org/licenses/MIT) 

<div class="centered">
<img src="./images/Logo-DAI-UDS-WWU-White.png" style="height:200px">
</div>

</div>


## References
