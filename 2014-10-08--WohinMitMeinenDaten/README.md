# Raw Data and Data Analysis - An improvement to archiving?
## Wohin mit meinen Daten?

This talk was held during the session:

### [Zum langfristigen Umgang mit Forschungsdaten in den Altertumswissenschaften](http://www.ianus-fdz.de/projects/veranstaltungen/wiki/Wohin_mit_meinen_Daten)

Sektion des DAI und IANUS beim 8. Deutschen Archäologiekongress
October, 8th, 2014

## Which data are in this repository?
  - [The slideshow](WMMD-StruplerBeamer.pdf)
  - [The images](images/)
  - [The text](WMMD-Text.pdf)


## How it works?

####  Presentation
Compiled with [XeLaTex](www.xelatex.org) 
 - Source: [WMMD-StruplerBeamer.tex](WMMD-StruplerBeamer.tex)
 - Output: [WMMD-StruplerBeamer.pdf](WMMD-StruplerBeamer.pdf)

Probably I should have ask myself the question "Wohin mit meinen Daten?" before preparing the presentation. At that time, I didn't think about publishing it online as .html. I started with a Rmarkdown file [WMMD-pdfbeamer.RMd](WMMD-pdfbeamer.RMd), compiled with   [RStudio](www.rstudio.com) Version 0.98.1091 to obtain [WMMD-pdfbeamer.tex](WMMD-pdfbeamer.tex). As I didn't know enough of Markdown (html and css) to produce what I wanted so I tweaked the .tex file to have a nice beamer.pdf

<br>
Sketch of the road:

[WMMD-pdfbeamer.RMd](WMMD-pdfbeamer.RMd)
 ---->
[WMMD-pdfbeamer.tex](WMMD-pdfbeamer.tex) 
 ---->  (handiwork)  ---->
[WMMD-StruplerBeamer.tex](WMMD-StruplerBeamer.tex)

####  Text
Compiled with [RStudio](www.rstudio.com) Version 0.98.1091 
- Source: [WMMD-Text.RMd](WMMD-Text.RMd)
- Output: [WMMD-Text.tex](WMMD-Text.tex) and [WMMD-Text.pdf](WMMD-Text.pdf)
