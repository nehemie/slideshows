\documentclass[]{article}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
\else % if luatex or xelatex
  \ifxetex
    \usepackage{mathspec}
    \usepackage{xltxtra,xunicode}
  \else
    \usepackage{fontspec}
  \fi
  \defaultfontfeatures{Mapping=tex-text,Scale=MatchLowercase}
  \newcommand{\euro}{€}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\usepackage[margin=1in]{geometry}
\usepackage{graphicx}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
\ifxetex
  \usepackage[setpagesize=false, % page size defined by xetex
              unicode=false, % unicode breaks when used with xetex
              xetex]{hyperref}
\else
  \usepackage[unicode=true]{hyperref}
\fi
\hypersetup{breaklinks=true,
            bookmarks=true,
            pdfauthor={Nehemie Strupler},
            pdftitle={Raw Data and Data Mining. An improvement to archiving?},
            colorlinks=true,
            citecolor=blue,
            urlcolor=blue,
            linkcolor=magenta,
            pdfborder={0 0 0}}
\urlstyle{same}  % don't use monospace font for urls
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\setcounter{secnumdepth}{0}

%%% Use protect on footnotes to avoid problems with footnotes in titles
\let\rmarkdownfootnote\footnote%
\def\footnote{\protect\rmarkdownfootnote}

%%% Change title format to be more compact
\usepackage{titling}
\setlength{\droptitle}{-2em}
  \title{Raw Data and Data Mining. An improvement to archiving?}
  \pretitle{\vspace{\droptitle}\centering\huge}
  \posttitle{\par}
  \author{Nehemie Strupler}
  \preauthor{\centering\large\emph}
  \postauthor{\par}
  \predate{\centering\large\emph}
  \postdate{\par}
  \date{Wohin mit meiner Daten? \textbar{} Thursday, October 08, 2014}




\begin{document}

\maketitle


{
\hypersetup{linkcolor=black}
\setcounter{tocdepth}{2}
\tableofcontents
}
\section{Introduction}\label{introduction}

\subsection{Self-promotion}\label{self-promotion}

Let me start by saying just a few words about my background. I am 27
years old and PhD candidate at the universities of Strasbourg (France)
and Münster (Germany). Actually I have a half time position since May at
the Istanbul branch of the German Archaeological Institut. This is my
first talk on this topic and some thoughts are still not very ripe.
Please interrupt me if there's something which needs clarifying. I will
enjoy discussing some of the points at the end of my presentation.

\subsection{Archaeology has an ``ethic of preservation'' (Harley et al.
2010).}\label{archaeology-has-an-ethic-of-preservation-31harley2010-31.}

Recording and archiving are imperative in archaeology. In the course of
excavations, sites are destroyed and the excavation (``the experiment'')
cannot be repeated. Everything that has not been seen or recorded is
lost forever. In recent years, amounts and types of collected data have
dramatically increased. These data vary depending on the scope and the
aim of the research, the methods involved in collection and preservation
as well as funding. In a nutshell, all archaeological projects produce
huge amounts of data in numeric form of data, that have become with
improvements in technology and tools easier to collect, manage, archive,
distribute and reuse.

Archaeology needs empirical observations to draw a broader picture
beyond its own research. The use of archives and other's data are a
\emph{sine qua non} in archaeological research. In recent years, more
and more funding bodies require or encourage long term archiving and
releasing of data. These policies ``are predicated on the assumption
that those data are useful to others'' (Wallis, Rolando, and Borgman
2013). Implicitly suggested is the release of ``raw'' data from a
research program and not only the results currently published in
articles or books. These policies postulate that release of raw data in
the public domain:

\begin{itemize}
\itemsep1pt\parskip0pt\parsep0pt
\item
  improves understanding, control and verification of the analysis
  (keyword \textbf{reproducibility})
\item
  stimulates new research questions by others on the same data-set
\item
  promotes new field of study (keywords \textbf{Advancing research} and
  \textbf{innovations})
\item
  Latent and mostly unspecified is the idea that the release of data,
  maintain data attractive and available as a basis for new and
  inventive research. I will turn back to this point at the end of my
  presentation.
\end{itemize}

In contrast with the aspect of publishing, little research has been done
into and with the reuse of archaeological raw data. Nevertheless if
policies to archive data are ``predicated on the assumption that those
data are useful to others'' (Wallis, Rolando, and Borgman 2013) then
only high quality data are addressed. If data are poor formatted or of
poor quality, they won't be used.

\subsection{Outline}\label{outline}

The aim of my paper is to show how statistical analysis could be used
side by side with the creation of raw data. This practice will ensure
the coherence of data prior to editorial and archiving work. Firstly I
will focus on the nature of raw data to archive and reuse. Then I will
draw your attention to the creation of raw data in an archaeological
project. Finally I will show the role of statistic in the process to
create data that are archivable.

\section{The life cycle of raw data}\label{the-life-cycle-of-raw-data}

I start this presentation by stressing the difference between raw data
and open data. A general definition of raw is

\subsection{Being in or nearly in the natural
state}\label{being-in-or-nearly-in-the-natural-state}

Raw data are information that is gathered for a research study before
that information has been transformed or analyzed in any way. The term
can apply to the data as soon as they are gathered or after they have
been cleaned, but not in any way further transformed or analyzed. The
expression raw data implies that these data cannot provide useful
information without further effort. I will chiefly speak of raw data and
not from open data. Raw data, if published in an open archive, will turn
into open data. But open data are not necessarily raw data (like
summaries for example)

Raw data must not be analyzed by the collector. Collector and processor
may be different. If they are different, some questions must be assessed
prior to the analyze of the data. As for every research, raw data are
collected with a purpose. Research questions motivate the collection and
the exclusion of other information (e.g.~do we need to record the
weather). Quality of the data must be assessed (degree of error,
accuracy, representativeness and completeness). Assessments include
establishing the reliability and validity of data, justifications for
discarding some of the data and consideration of the problem of missing
data.

If a researcher processes his own or another's raw data, the difficulty
are the same. ``The challenge for most researchers who collect and
analyze data is to extract useful information from the raw data they
start with.'' (McIver 2008)

\subsection{Why archive and publish
data?}\label{why-archive-and-publish-data}

After presenting succinctly what are raw data, we can address the
reasons of archiving and publishing these data Research on sharing,
reuse and archiving of data shows that until nowadays there is little
attention from a researcher to publish and archive (raw) data. Diane
Harley (2013, 80--82) indicates some reasons of this phenomenon:

\subsubsection{Publishing of raw data
is:}\label{publishing-of-raw-data-is}

\begin{itemize}
\itemsep1pt\parskip0pt\parsep0pt
\item
  Not rewarded: ``individual imperatives for career self-interest,
  advancing the field, and receiving credit are often more powerful
  motivators in publishing decisions than the technological affordances
  of new media.''
\end{itemize}

\subsubsection{Archiving of raw data
is:}\label{archiving-of-raw-data-is}

\begin{itemize}
\itemsep1pt\parskip0pt\parsep0pt
\item
  Not rewarded: ``Stand-alone cataloging or curating, protocols are not
  weighted as heavily as peer-reviewed ``interpretive'' work
\item
  Time consuming ``Most scholars {[}\ldots{}{]} are plagued with a lack
  of time; are in need of more, not fewer, filters and curated
  sources;''
\end{itemize}

According to surveys and my personal experience, we can still state that
there is still little change in publishing and archiving raw data. But
is that a very astonishing statement?

\subsection{The sociotechnical system
concept}\label{the-sociotechnical-system-concept}

The points made by Wallis and others can be better understood within the
concept of sociotechnical system, as firstly developed by Thomas P.
Hughes, Brian Pfaffenberger and other in the late 80s and early 90s.
This concept explicitly conceives professional status, education,
practical experience, economic and institutional interests, as socials
forces that shape technology. Pfaffenberger notes that, ``those who seek
to develop new technologies must concern themselves not only with
techniques and artifacts {[}In our case techniques are, let's say,
repository and artifacts, data{]}; they must also engineer the social,
economic, legal, scientific, and political context of the technology. A
successful technological innovation occurs only when all the elements of
the system, the social as well as the technological, have been modified
so that they work together effectively'' (Pfaffenberger 1992, 498).

To obtain a change in the practice of producing, publishing and
archiving data, it is not just about resolving technics and
technological problems and I will quote Pfaffenberger again: ``To
construct a sociotechnical system is not merely to engage in some
creative or productive activity. It is to bring to life a deeply desired
vision of social life'' (Pfaffenberger 1992, 506).

\section{Archaeological raw data}\label{archaeological-raw-data}

After an introduction on the raw data and the sociotechnical concept, we
can now address the production of archive and reuse of data. In an
article published in June, Eric C. Kansa, Sarah Whitcher Kansa and
Benjamin Arbuckle criticized the way archives are chiefly use, and urged
to extend archiving goals beyond ``preservation for the sake of
preservation'' (E. C. Kansa, Kansa, and Arbuckle 2014, 58).

\subsection{The Publishing and Pushing Model (E. C. Kansa, Kansa, and
Arbuckle 2014)}\label{the-publishing-and-pushing-model-kansa2014}

To improve the actual practices they started a pilot project. They
gathered and aggregated multiple data-set about archaeozoology in Turkey
in order to tackle the origins and spread of domesticated animals in the
Neolithic Turkey. From their own experience, they drew a model for data
management divided in 5 steps to move from separated data set to an
analyze

\begin{itemize}
\itemsep1pt\parskip0pt\parsep0pt
\item
  Step 1: Solicitation and metadata documentation
\item
  Step 2: Review, decoding and editing of data
\item
  Step 3: Annotation and alignment
\item
  Step 4: Contributor review, peer review, and analysis
\item
  Step 5: Publication, indexing and archiving
\end{itemize}

We can't discuss the model now, but I would like to stress only one
point made by the authors. They explain the characteristics and roles of
``data editors'' (E. C. Kansa, Kansa, and Arbuckle 2014, 66). The
data-editors play a major role between the collection of data and the
archiving

\begin{enumerate}
\def\labelenumi{\arabic{enumi})}
\itemsep1pt\parskip0pt\parsep0pt
\item
  Work of Data editors:
\end{enumerate}

\begin{itemize}
\itemsep1pt\parskip0pt\parsep0pt
\item
  create supplemental documentation (because they wanted to understand
  the data-sets)
\item
  make annotations
\item
  decodes, control the data-set
\item
  ask more information to the data collector to complete and correct the
  documentation
\end{itemize}

\begin{enumerate}
\def\labelenumi{\arabic{enumi})}
\setcounter{enumi}{1}
\itemsep1pt\parskip0pt\parsep0pt
\item
  Requirement for data editors
\end{enumerate}

\begin{itemize}
\itemsep1pt\parskip0pt\parsep0pt
\item
  domain knowledge
\item
  management knowledge
\end{itemize}

Data-editors play a key role in the process of archiving and reusing
data and that is something that is not really used in an archaeological
project. The authors concede that ``The approach advocated is the first
of its kind involving the large-scale, digital publication and
integration of archaeozoological data-sets and provides a template for
future archaeological collaborations.'' (B. S. Arbuckle et al. 2014, 9).
What are the differences with an archaeological project? I argue that
that all the data-sets used in the study have been created with the
knowledge to be analyzed with some statistic. In Archaeozoology, you
cannot publish ``raw data''. In an archaeozoological project, the one
who produces the data-set knows (more or less) what will happen with the
data. You need to obtain an NISP (number of identified specimens),
species frequencies, biometrics and survivorship. Just these basic
statistic ensure more quality and control of the data-set. But is it the
case in an archaeological project?

\subsection{The \textbar{} exquisite \textbar{} corpse \textbar{} shall
drink \textbar{} the \textbar{} new \textbar{}
wine}\label{the-exquisite-corpse-shall-drink-the-new-wine}

The Exquisite Corpse game was developed by the artists and writers
associated with Andre Breton's surrealist group during the third decade
of the twentieth century. This is a method by which a collection of
words or images is collectively assembled. Player uses predetermined
patterns to build sentences, like the pattern ``The \textbar{}
\textbf{adjective} \textbar{} \textbf{noun} \textbar{} \textbf{verb}
\textbar{} the \textbar{} \textbf{adjective} \textbar{} \textbf{noun}''.
Each collaborator adds to a composition in sequence by following the
rule (i.e.~either adjective, noun or verb). The name of the game (``The
exquisite corpse shall drink the new wine'' (originally ``Le cadavre
exquis boira le vin nouveau'') is derived from the first phrase that
resulted when surrealists played the game.

In an exaggerated way, a lot of acquisitions of archaeological data are
done this way. The results of the game, a data frame with variables and
observations (each new sentence) looks like a data frame from a
database. In an archaeological project people fill blanks according to
standardized form in the right way (free text, number, selection in a
list and so on). And, no one ever see the whole picture, at least when
you are doing it. You may end up with the graphical version of the
exquisite corpse, in which you only know what was drawn before your
turn.

I don't critic this process, because this process in archaeology cannot
be avoided, due to the nature of the observation. Before and during an
archaeological project or a survey, you can never know, what will be
founded. It is impossible to come with a classification like the one of
the elements that did Dmitir Mendeleev, in which he leaves gaps in the
periodic table when it seemed that the corresponding element had not yet
been discovered. If you come with predefined set of recording, you
missed important information as stated by Pollock and Bernbeck: ``our
standard, taken-for-granted categorizations of material remains served
to reproduce an existing way of thinking about the past and exclude
other possibilities'' (Pollock and Bernbeck 2010, 42)

\section{Raw data}\label{raw-data}

During the course of excavation, the collection of data cannot be very
clean. Let me just run over the key point again:

\begin{itemize}
\itemsep1pt\parskip0pt\parsep0pt
\item
  Overall management problem
\item
  Collectors are not aware of data management problems and the long term
  life cycle of raw data
\item
  Processes of collection is heuristic: they will never be one way to do
  it. That's the categorization pitfall.
\end{itemize}

\subsection{Open data ``in the natural
state''?}\label{open-data-in-the-natural-state}

Archiving and release of raw data don't solve the problem. If data
aren't clean and if they are badly formatted they can't or won't be
used. And there is a real need to make this kind of analysis. In this
slide, a screenshot of archived and published data, you can see, I would
say, a rather typical ``natural state'' of archaeological data. The
results, a uni-variate exploration of the variable ``Dates'' of 3015
ceramic shows:

\begin{itemize}
\itemsep1pt\parskip0pt\parsep0pt
\item
  There will be a big problem of compatibility with other data-set
  (so-called alignment of data)
\item
  A sad observation: it's a proof no one really tried to do something
  with these data. That's will never be rewarded
\item
  Everything was recorded in a database, but where is the gain. That's
  time consuming
\end{itemize}

Even with a lot of meta-data it is really hard to use this kind of data.
A firsthand experience with raw data and an analyze is necessary and
cannot not be substituted by any amount of metadata (Mattingly and
Witcher 2004, 177). What we need are:

\begin{itemize}
\itemsep1pt\parskip0pt\parsep0pt
\item
  raw data
\item
  a script to clean the data (so you never alter them)
\item
  a first analyze
\end{itemize}

I think, that is a good practice for archiving and for dissemination:
you need to understand the data set, correct the error, look at the
missing data and describe the data-set.

\includegraphics{./images/Raw-DataToFirstAnalyse.png}

\subsection{Statistic as a ``bottom up''
process}\label{statistic-as-a-bottom-up-process}

Making basic uni-, bi- and multivariate analyses allow cleaning data and
making basic data analyses. Analyses should not come just at the end of
the process but during the process of collecting (fig.). Management
decides which and how data have to be collected, collectors generate the
raw data and editor analyze data. But everyone influences the other or
must give feedback to the others. Doing some statistic with the
collection of the data and prior to archiving is another way to change
research practices and raw data management. If raw data and their
management become a mean of research (among others), and you use it,
then it cannot be seen as a ``not rewarded'' or ``time consuming''. The
British anthropologist Alfred Gell stated ``\emph{the technology of
enchantment} is founded on the \emph{enchantment of technology}. The
enchantment of technology is the power that technical processes have of
casting a spell over us so that we see the real world in an enchanted
form'' (Gell 1992, 44). With other social changes (funding, education,
reward, example of ``advancing research'' and so on) we can expect to
change the sociotechnical concept to reach and attract a broader
community. Changing how raw data are perceived, will increase the
quality and management of data. Moving raw data to a key role in
research has the power to shape the perception of archiving and
publishing data.

\includegraphics{./images/DataEditor.png}

\section{Conclusion}\label{conclusion}

Briefly, to conclude, let me just run over the key point again.

\begin{itemize}
\itemsep1pt\parskip0pt\parsep0pt
\item
  There is a confusion between open and raw data
\item
  Innovation is technical and social
\item
  The role of data editor is underestimated
\item
  There is still a lot of production of raw data that are (and will)
  never be analyzed .. and that is not casting a spell over us
\item
  Data mining has a key role to play in the perception of raw data
\end{itemize}

\section*{References}\label{references}
\addcontentsline{toc}{section}{References}

Arbuckle, Benjamin S., Sarah Whitcher Kansa, Eric Kansa, David Orton,
Canan Çakırlar, Lionel Gourichon, Levent Atici, et al. 2014. ``Data
Sharing Reveals Complexity in the Westward Spread of Domestic Animals
Across Neolithic Turkey.'' \emph{PLoS ONE} 9 (6). Public Library of
Science: e99845.
doi:\href{http://dx.doi.org/10.1371/journal.pone.0099845}{10.1371/journal.pone.0099845}.
\url{http://dx.doi.org/10.1371\%2Fjournal.pone.0099845}.

Gell, Alfred. 1992. ``The Technology of Enchantment and the Enchantment
of Technology.'' In \emph{Anthropology, Art and Aesthetics}, edited by
J. Coote and A. Shelton, 40--66. Oxford: Clarendon.

Harley, Diane. 2013. ``Scholarly Communication: Cultural Contexts,
Evolving Models.'' \emph{Science} 342 (6154): 80--82.
doi:\href{http://dx.doi.org/10.1126/science.1243622}{10.1126/science.1243622}.

Harley, Diane, Sophia Krzys Acord, Sarah Earl-Novell, Shannon Lawrence,
and C. Judson King. 2010. ``Assessing the Future Landscape of Scholarly
Communication: An Exploration of Faculty Values and Needs in Seven
Disciplines.'' Center for Studies in Higher Education, UC Berkeley.
\url{http://escholarship.org/uc/item/15x7385g}.

Kansa, Eric C., Sarah Whitcher Kansa, and Benjamin Arbuckle. 2014.
``Publishing and Pushing: Mixing Models for Communicating Research Data
in Archaeology'' 9 (1): 57--70.
doi:\href{http://dx.doi.org/10.2218/ijdc.v9i1.301}{10.2218/ijdc.v9i1.301}.

Mattingly, D. J., and R. E. Witcher. 2004. ``Mapping the Roman World :
the Contribution of Field Surveydata.'' In \emph{Side-by-Side Survey :
comparative Regional Studies in the Mediterranean World}, 173--86.
Oxford: Oxbow Books.

McIver, John P. 2008. ``Raw Data.'' In \emph{Encyclopedia of Survey
Research Methods}, edited by Paul J.Lavrakas, 693--94. Sage.

Pfaffenberger, Bryan. 1992. ``Social Anthropology of Technology.''
\emph{Annual Review of Anthropology} 21 (1): 491--516.
doi:\href{http://dx.doi.org/10.1146/annurev.an.21.100192.002423}{10.1146/annurev.an.21.100192.002423}.

Pollock, Susan, and Reinhard Bernbeck. 2010. ``An Archaeology of
Categorization and Categories in Archaeology.'' \emph{Paléorient} 36
(1): 37--47.

Wallis, Jillian C., Elizabeth Rolando, and Christine L. Borgman. 2013.
``If We Share Data, Will Anyone Use Them? Data Sharing and Reuse in the
Long Tail of Science and Technology.'' \emph{PLoS ONE} 8 (7). Public
Library of Science: e67332.
doi:\href{http://dx.doi.org/10.1371/journal.pone.0067332}{10.1371/journal.pone.0067332}.
\url{http://dx.doi.org/10.1371\%2Fjournal.pone.0067332}.

\end{document}
