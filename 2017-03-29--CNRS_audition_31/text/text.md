---
title: "La ville basse de Boğazköy au IIe millénaire av. J.-C."
author: "Néhémie"
date: "Vendredi 16 septembre, 2016"
output: pdf_document
geometry: a5paper
fontsize: 16pt
---
 
Bonjour à tous et merci de m’auditionner aujourd'hui.  
 
Je désirerais, avant de présenter l'élaboration de mon projet sur la perception
de territoire en préhistoire, de ses méthodes et de ses objectifs, introduire
mon sujet par un exemple d'actualité. Nous le savons, les cartes sont
subjectives, de point de vue de l'émetteur, du récepteur et le service privé
Google Maps montre assez clairement comment une carte se conçoit différemment
selon à qui on s’adresse. Sur cette diapositive, quatre image de la Crimée,
trois à partir du service de Google, en bas à gauche tiré du service libre OSM
(Open Street Map). Si l'on se connecte au service ukrainien (en haut gauche), il
n'existe pas de frontière entre la Crimée et l'Ukraine, la frontière est nette
dans le service russe (Frontière Russe/Ukraine), en traitillé pour le reste du
monde. Le service OSM, géré par une communauté et qui ne propose qu'une seule
version, ne représente pas de frontière (simplement démarcation des provinces).
Représentation de l'espace par une carte, deux dimensions conceptuelles
s'affrontent, le scientifique et la rhétorique. Toute carte est bien sur une
construction scientifique, elle repose sur des faits, les informations y sont
traitées rigoureusement et représentées selon les règles graphiques
scientifiquement établies. Toute carte serait une construction sociale
intrinsèquement subjective. En effet, chaque carte porte en elle un discours,
une idéologie. Celle-ci s’exprime par le choix des couleurs, le tracé des
frontières, les mots choisis, les titres, la hiérarchisation de l’information,
les exagérations et les omissions. En somme, il y a les faits et la façon dont
on les raconte.


Lorsque l'on se penche sur une exemple tiré de la proto-histoire, la situation
est nettement plus frustres. Il est rare de trouver des cartes très divergentes,
et la représentation de royaume se calque sur celui de nation. 





--> Ccl: Recherche qui est importante car à l'inverse de ce que pourrait
suggérer mon introduction qui s'appuyait sur une phénomène contemporain pour
introduire un état passé, il me semble que le questionnement du passé et de sa
définition de territoire tout comme de sa visualisation à un énorme potentiel
pour le futur. Who controls the past, controls the future.


Question: cartographie relation: UMS RIATE, UMR Géographie-cités
